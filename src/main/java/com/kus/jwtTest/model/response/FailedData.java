package com.kus.jwtTest.model.response;

public class FailedData {
	
	private String errCode;
	private String errDescr;
	
	public String getErrCode() {
		return errCode;
	}
	public void setErrCode(String errCode) {
		this.errCode = errCode;
	}
	public String getErrDescr() {
		return errDescr;
	}
	public void setErrDescr(String errDescr) {
		this.errDescr = errDescr;
	}
	
	

}
