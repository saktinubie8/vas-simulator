package com.kus.jwtTest.model.response;

public class FailedResponse extends BaseJwtResponse{
	
	private FailedData data;

	public FailedData getData() {
		return data;
	}

	public void setData(FailedData data) {
		this.data = data;
	}
	
	

}
